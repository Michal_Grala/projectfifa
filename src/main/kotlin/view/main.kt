package view

import controller.CSVFileReader
import controller.PlayerMapper
import model.Player

fun main() {

    val reader = CSVFileReader()
    val lines = reader.read("D:\\Pliki\\Zeszyt1.csv")
    val players = lines.map { PlayerMapper.map(it) }

    players.forEach {
        println(it)

    }

}