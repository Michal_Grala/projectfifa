package controller

import java.io.File

class CSVFileReader {
    fun read(path: String): ArrayList<String> {

        val file = File(path)
        val lines = ArrayList(file.readLines())
        lines.removeAt(0)

        return lines
    }
}




