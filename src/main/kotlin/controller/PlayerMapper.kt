package controller

import model.Player

object PlayerMapper {
    fun map(line: String): Player {

        val tab = line.split(";")

        val player = Player(
            id = tab[0].toInt(), //INT
            gameId = tab[1],
            name = tab[2],
            age = tab[3].toInt(), //INT
            photo = tab[4],
            nationality = tab[5],
            flag = tab[6],
            overall = tab[7].toInt(), //INT
            potential = tab[8],
            club = tab[9],
            club_Logo = tab[10],
            value = MoneyFormatter.format(tab[11]),
            wage = MoneyFormatter.format(tab[12])
        )
        return player
    }

}

