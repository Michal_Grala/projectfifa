package controller

object MoneyFormatter {

    fun format(value: String): Int {

      try {
          if (value.isEmpty()) {
              return 0
          }
          val cleanValue = value.trim().removeRange(0, 1)

          val multiple = when (cleanValue.last()) {

              'K' -> 1000
              'M' -> 1000000
              else -> 1

          }

          var result = cleanValue.substring(0,cleanValue.length - 1).toDouble() * multiple


          return result.toInt()
      }catch (e: Exception){
          e.printStackTrace()
          return 0
      }

    }


}