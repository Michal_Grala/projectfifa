package model

data class Player (

    val id: Int,//INT
    val gameId: String,
    val name: String,
    val age: Int,//INT
    val photo: String,
    val nationality: String,
    val flag: String,
    val overall: Int,//INT
    val potential: String,
    val club: String,
    val club_Logo: String,
    val value: Int,
    val wage: Int
)
